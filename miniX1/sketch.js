var song;
var img1,img2;

function preload() {
  song = loadSound("spacesound.mp3");
  img1 = loadImage("blackhole.png");
 	img2 = loadImage("ufo.png");
}

function setup() {
  createCanvas(800, 466);
  //play sound
  song.play();
  song.setVolume(0.5)
  //backgroud pic in center
  imageMode(CENTER);
  noStroke();
}

function draw() {
  background(255);

//blackhole
  image(img1,400,233);

//moving ufo
imageMode(CENTER);
  image(img2,mouseX,mouseY);
}
//function if mouse is pressed (deletes)
function mousePressed() {
  background(160,50,50);
  }
