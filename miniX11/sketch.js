let power = [];
let mode;

function preload() {
	myFont = loadFont('assets/RobotoMono-VariableFont_wght.ttf');
}

function setup() {
	mode = 1;
	createCanvas(windowWidth, windowHeight);
}

function mousePressed() {
	if (mode==1) {
		mode = 2;
		for (let i = 0; i < 50; i++) {
			power[i] = new Power(random(width), random(height));
		}
	} else if (mode==2) {
		power.push(new Power(mouseX, mouseY));
	}
}

function draw() {
background(255,120);

	if (mode==1) {
		push();
		fill(0);
		textSize(20);
		textAlign(CENTER);
		textFont(myFont);
		text('Press anywhere to initiate the ecosystem',width/2,height/2);
		pop();
	}

	if (mode==2) {
		for (let i = 0; i < power.length; i++) {
			power[i].development();
			power[i].labor();
			for (let j = 0; j < power.length; j++) {
				if (i != j && power[i].exchange(power[j])) {
					if (power[i].size < power[j].size) {
						power[j].size += 0.01;
						power[i].size -= 0.01;
					} else if (power[i].size > power[j].size) {
						power[i].size += 0.01;
						power[j].size -= 0.01;
					}
					if (power[i].size > width){
						noLoop();
						console.log('THE END?');
					} else if (power[j].size > width){
						noLoop();
						console.log('THE END?');
					}
				}
			}
		}
	}
}
