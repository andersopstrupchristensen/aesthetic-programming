// my second miniX for AP
// by Anders Opstrup
let col = (249);
let r = (255);
let g = 50;
let b = 0;
var img1;

function preload() {
  img1 = loadImage("cloud.png");

}

function setup() {
  // put setup code here
  createCanvas(1200, 900,);
  frameRate(30);

}

function draw() {
//background blue Color (sky) ((R),(G),(B))
background(172, 242, 245);

//emoji one
// face
fill(255,col,0);
ellipse(900, 700, 290, 290); // Inner gray ellipse
col = map (mouseX,0,1200,0,900);
r = map(mouseX,0,1200,0,900);
g = map(mouseX,0,1200,0,900);
b = map(mouseX,0,1200,0,900);

// eyes
fill(255,250,255);
ellipse(850, 675, 40, 40);

fill(255,250,255);
ellipse(950, 675, 40, 40);

// Smile
arc(900, 750, 90, 90, 0, PI, CHORD);

//emoji two
//sun ellipse
push();
noStroke()
fill(249,208,0);
ellipse (1000,200,100,100);
pop();

// sun lines
fill(249,208,0);
line(959,234,928,280);
line(946,194,916,201);
line(957,164,929,125);
line(991,140,991,78);
line(1039,160,1081,123);
line(1040,236,1075,275);
line(1001,255,1001,291);
line(1056,201,1096,203);

//sun evil eyes
fill(249,79,00)
ellipse(985,190,10,10)

arc(1015, 190, 10, 10, 0, PI, CHORD);

//sun evil mouth
arc(1000, 219, 30, 10, 0, PI, CHORD);

//moving cloud
imageMode(CENTER);
  image(img1,mouseX,mouseY);
}
