# third minix

RUNME: https://andersopstrupchristensen.gitlab.io/aesthetic-programming/miniX3

CODE:  https://gitlab.com/andersopstrupchristensen/aesthetic-programming/-/blob/master/miniX3/sketch.js


What do you want to explore and/or express?

In this miniX assignment I have been working on making a throbber. This time, I spent a long time looking online to find inspiration for my throbber - we all know the classic throbbers which often is some kind of shape that rotates around its own axis. In my online search I found many different takes on what a throbber might look like. I especially liked the throbbers that fill the entire screen, and which stands out from the more classic rotary throbbers. I decided to try and make a throbber that filled the entire screen because I thought they were more exciting than a small rotating wheel. A throbber is often something I associate with some form of waiting and frustration that things are not going as fast as I would like. Although the waiting time often lasts only a few seconds, it usually feels like a waste of time and for some reason I always get wildly annoyed when I encounter a throbber. I therefore decided to make a throbber which was more exciting to look at and which also changes every time you see it. The idea was that this throbber should trigger less frustration because it always changes a bit and therefore is more exciting to encounter. The idea was to make a throbber which made it very clear how far one was in the loading process because I thought it would create less frustration. The thought was to make a white canvas with randomly appearing black lines which should cover the entire background until the canvas had turned black. 
The throbber developed further the longer I came in my process, and I got the idea that it could be fun if you made a throbber or loading sequence that was never finished and instead of making a Throbber that could the level of frustrations, I actually ended up making a Throbber that can only end in frustration. 

![](screenshot.png) 
![](screenshot1.png) 

What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way? How is time being constructed in computation (refer to both the reading materials and your coding)?

I have created the program by setting my variables to the Beziers (curved lines) that randomly pop up on the screen. Subsequently I have made the actual lines in the function draw. I chose to use setup draw precisely because setup draw acts as a loop, which makes it possible to loop the lines of code that make the lines. I have chosen to use function draw because it allows me to adjust how fast I want the lines appear. I can control the pace of how fast the lines appears using the frameRate (); function in function Setup. I have chosen a lower pace of frames per second (30fps) to make the lines appear a bit slower than if the framerate was set to the standard; 60 frames per second. Function Draw therefore acts as a loop that is updated 30 times per second. There is a very big difference in the understanding of time from people to computer. This is an aesthetic choice which makes it easier to see the different colors I have chosen, but also makes the throbber seem to load more slowly. A throbber is an instrument that is often used to hide some underlying actions in the code. Throbbers keep ordinary people at a distance from the code, and thus also what data is used and passed on. As mentioned earlier, a throbber also has an impact on the human perception of time. Time can be considered as something we humans have created to get a systemic view of when and why things happen. Humans understanding of time as something that passes is thus created to give meaning to something that is very complex. “Humans have developed a time experience which was linked to natural life cycles, but it has been influenced by both technology and social conditions.” - Lammerant, The Techno-Galactic Guide to Software Observation, p.89, (2018) Like humans, computers also have their own way of calculating time, but time in a computer is contrary to the human perception of time.  




