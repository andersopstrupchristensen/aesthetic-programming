// variables for the bezier
let startX;
let startY;
let endX;
let endY;
// variables for the colors
let r;
let g;
let b;

function setup() {
createCanvas(windowWidth,windowHeight);
frameRate(30);

startX = random(width);
startY = random(height);
endX=random(width);
endY=random(height);

r = random(255);
g = random(255);
b = random(255);

}

function draw() {

//background(220, 150, 20);
// this is the variables that creates the random colors
r += random(-5, 5);
g += random(-5, 5);
b += random(-5, 5);

// this is what makes the bezier appear random on the canvas
startX = random(width);
startY = random(height);
endX=random(width);
endY=random(height);

// this is the bezier "curved lines"
noFill();
stroke(r, g, b, 80);
bezier(0, 0, startX, startY, endX, endY, width, height);
bezier(1, 100, startX, 0, 100, startY, width, height);
bezier(startY, 10, startX, 1300, endX, 420, width, height);
bezier(90, 120, startX, startY, endX, endY, width, height);
bezier(startY, 150, startX, 100, endX, 0, width, height);
bezier(900, 200, startX, startY, endX, endY, width, height);
bezier(250, 2, startX, startY, endX, endY, width, height);
bezier(620, startY, startX, 1300, endX, endY, width, height);
bezier(0, startY, startX, 1300, endX, 0, width, height);

// text
let s = 'please wait forever for this page to fill';
fill(50);
text(s, width/2, height/2);

}
