let capture;
let ctracker;

function setup() {
  createCanvas(windowWidth,windowHeight);
  //video
  capture = createCapture(VIDEO);
  capture.size(width,height);
  capture.hide();

  //face ctracker
  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);

}

function draw() {
  // hidden video capture
  image(capture, 2,0, width, height);

  background(0);

  for(let x =0; x<=width; x+=100 ){
    for(let y =0; y<=height; y+=30 ){
    textSize(32);
    fill(255);
    text('DATA',x,y);
      }
}
//box that covers 'DATA DATA'
  fill(0);
    rect(600, 274, 180, 27);

// text 'YOUR DATA'
  textSize(32);
  fill(162,61,39);
  text('YOUR DATA', 601, 300);


  faceTracker();
}


function faceTracker(){
  //facetracker
     let positions  = ctracker.getCurrentPosition();
       for(let i=0; i< positions.length; i++) {
         noStroke();
         fill(255,0,0);
         ellipse(positions[i][0], positions[i][1], 9,9);
         fill(0);
           rect(602, 274, 180, 27);

       }
}
