let s;

let r;
let g;
let b;

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);
  frameRate(60);
  s = 0;

  r = random(255);
  g = random(255);
  b = random(255);
}

function draw() {
  translate(width/2, height/2);
//this for loop creates the circular shape
r += random(-205, 205);
g += random(-205, 205);
b += random(-205, 205);
  beginShape();
  stroke(r,g,b, 10);
  noFill();
  for (let i = 0; i < 3600; i++) {
    let a = map(i, 0, 1000, 0, TWO_PI);
    let rad = 600 * noise(i * 0.03, s * 0.05);
    let x = rad * cos(a);
    let y = rad * sin(a);
    vertex(x, y);
  }
  endShape();

  s += 0.5;

  // clear the background every 1000 frames using mod (%) operator
  if (frameCount % 1000 == 0) {
	background(0);
  }

}
