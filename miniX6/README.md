**MINIX6**

[RUNME](https://andersopstrupchristensen.gitlab.io/aesthetic-programming/miniX6/)

[MYCODE](https://gitlab.com/andersopstrupchristensen/aesthetic-programming/-/blob/master/miniX6/sketch.js)

**MINIX6 REVISIT THE PAST**

In this week's miniX I have looked at one of my previous miniX assignments. I spent a long time figuring out which miniX I thought was worth working on again. I decided to take a look at my miniX3 task which was to design a throbber. I made this decision because I felt that the program I had made never quite turned out the way I would have liked it to be. At the same time I liked the aesthetics I had managed to create, simply by using the line function in combination with variables that randomly changed colors of the lines. My goal with my miniX3 task was to make a throbber that would gradually fill up the screen so it looked like something was loading. However the screen should never be completely filled, so that you could basically wait forever for the "loadingsequence" to finish. What I found most problematic about the program was that I felt that there was a lack of symmetry in the way the screen was filled. I therefore decided to work on making my throbber fill the screen in a more symmetrical way. In this way I make it clear that the "loading sequence" continues forever, precisely because the pages of the canvas are never filled with lines. I've changed the most central part of the code to make the visual expression more streamlined and to better fit my original idea. Instead of using the variables StartX, StartY, endX and endY I have now made a single variable which is based on P5.js noise function. This gives the lines a more fluid motion while still creating lines at random locations on the canvas.

In the study group, we had a long talk about how we should approach the task we all felt that the task was a bit abstract and that it is often very difficult to be critical of one's own tasks, Jacob therefore suggests that I could choose one specific element from a previous miniX that I would like to remake. This way of approaching the task made it less abstract for me to figure out what I wanted to do and what task I wanted to look at again. This approach also fits very nicely into one of this week's main focuses; **CRITICAL MAKING**. I do not know if the program has improved significantly, but I have managed to change the aesthetic expression so that it fits better with my original idea.

**CRITICAL MAKING & AESTHETIC PROGRAMMING**

After working with critical making, it has become a lot easier for me to understand what the subject of aesthetic programming is really about. I have previously been very focused on the code behind my miniX tasks, and this has often made it difficult to be creative, as a result my reflections on how my program can affect the outside world have often felt coerced and this has made it difficult to write a reflective reedme. I see critical making as a kind of method, or a collection of methods that can be used to criticize my own conceptual process. Critical making can provide a understanding of the impact or consequences the creation of a specific product can have to people and/or society. Critical making is a reflected way of looking at concrete issues in design situations in order to better understand conceptual problems from a socio-technical perspective. I can therefore see many similarities between the course aesthetic programming and the methodology critical making. I found this quote very relevant in terms of my way of thinking in the course aesthetic programming: 

'Ultimately, critical making is about turning the relationship between technology and society from a "matter of fact" into a "matter of concern"'.(Ratto, Matt & Hertz, Garnet, 2019, "The Critical Makers Reader: (Un)Learning Technology", p. 20)

**MY MINIX3**
![](screenshot.png) 
**MY MINIX6**
![](screenshot1.png) 




