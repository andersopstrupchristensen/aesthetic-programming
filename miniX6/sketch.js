// variables
let startX;
let startY;
let endX;
let endY;

let r;
let g;
let b;

function setup() {
createCanvas(windowWidth,windowHeight);
background(255);
frameRate(60);
noiseSeed(90);
//stroke(10, 100);

r = random(255);
g = random(255);
b = random(255);

}
let xoff = 1.0;


function draw() {
  xoff = xoff + .01;
  let n = noise(xoff) * width;
  //line(n, 0, n, height);
  strokeWeight(10);
  stroke(r,g,b,100);
bezier(n, 20, n, 10, n, 90, n, 80);

  line(n, 0, n, 100);

  line(n, 0, n, 200);
  strokeWeight(5);
  stroke(100,140,200,100);

  line(n, 0, n, 300);
  strokeWeight(10);
  stroke(70,32);

  line(n, 0, n, 400);
  strokeWeight(1);
  stroke(4,210);

  line(n, 0, n, 500);
  strokeWeight(1);
  stroke(1,50);

  line(n, 0, n, 600);
  strokeWeight(1);
  stroke(10,160);

  line(n, 0, n, 700);
  strokeWeight(1);
  stroke(10,160);

  line(n, 0, n, 800);
  strokeWeight(1);
  stroke(10,255,160,20);

  line(n, 0, n, 900);
  strokeWeight(1);
  stroke(10,g);

  line(n, 0, n, 1000);
  strokeWeight(1);
  stroke(10,r);

  line(n, 0, n, 1020);
  strokeWeight(1);
  stroke(10,255);

}
