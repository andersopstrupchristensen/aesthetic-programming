 /*create a class: template/blueprint of objects
 with properties and behaviors*/
class Fuel {
    constructor()
    { //initalize the objects
    this.speed = floor(random(20, 2));
    //check this feature: https://p5js.org/reference/#/p5/createVector
    this.pos = new createVector(width+5, random(12, height/1.7));
    this.size = floor(random(15, 35));
    //rotate in clockwise for +ve no
    this.fuel_rotate = random(0, PI/20);
    this.emoji_size = this.size/1.8;
    }
  move() {  //moving behaviors for ellipse moving on X axis
    this.pos.x-=this.speed;  //i.e, this.pos.x = this.pos.x - this.speed;
  }
  show() { //fuel shown as ellipse
    push()
    translate(this.pos.x, this.pos.y);
    rotate(this.fuel_rotate);
    noStroke();
    fill(130, 120, 200, 200);//shadow
    ellipse(0, this.size, this.size, 1);
    fill(253,200,10); //front plane
    ellipse(0, 0, this.size, this.size, 200);
    pop();
 }
}
