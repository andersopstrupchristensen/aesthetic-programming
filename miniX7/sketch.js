/*based on ES6 (class-based object oriented programming is
introduced in ECMAScript 2015)
credit and inspiration:
game scene: ToFu Go by Francis Lam; emoji: Multi by David Reinfurt*/
let ufoSize = {
  w:100,
  h:100
};
let ufo;
let blackhole;
let ufoPosY;
let mini_height;
let min_fuel = 10;  //min fuel on the screen
let fuel = [];
let score =20, lose = 0;
let keyColor = 45;

function preload(){
  ufo = loadImage("ufo.png");
  blackhole = loadImage("blackhole.png");
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  imageMode(CORNER);
  ufoPosY = height/2;
  mini_height = height/2;
}
function draw() {
  background(240);

  image(blackhole,0, 0, windowWidth, 700);
  fill(keyColor, 255);
  rect(0, height/1.5, width, 1);
  displayScore();
  checkfuelNum(); //available fuel
  showfuel();
push();
  imageMode(CENTER)
  image(ufo, mouseX, mouseY, ufoSize.w, ufoSize.h);
pop();
  checkFuel(); //scoring
  checkResult();
}

function checkfuelNum() {
  if (fuel.length < min_fuel) {
    fuel.push(new Fuel());
  }
}

function showfuel(){
  for (let i = 0; i <fuel.length; i++) {
    fuel[i].move();
    fuel[i].show();
  }
}

function checkFuel() {
  //calculate the distance between each
  for (let i = 0; i < fuel.length; i++) {
    let d = int(
      dist(mouseX+ufoSize.w/2, mouseY+ufoSize.h/2,
        fuel[i].pos.x, fuel[i].pos.y)
      );
    if (d < ufoSize.w/2.5) {
      score++;
      fuel.splice(i,1);
    }else if (fuel[i].pos.x < 3) {
      lose++;
      fuel.splice(i,1);
    }
  }
}

function displayScore() {
    fill(keyColor, 160);
    textSize(17);
    text('You have collected! '+ score + " fuelobjects", 10, height/1.4);
    text('You have missed! ' + lose + " fuelobjects", 10, height/1.4+20);
    fill(keyColor,255);
    text(' USE THE MOUSE TO MOVE to collect fuelobjects',
    10, height/1.4+40);
}

function checkResult() {
  if (lose > score && lose > 2) {
    fill(keyColor, 255);
    textSize(26);
    text("YOU DID NOT COLLECT ENOUGH FUEL OBJECTS..GAME OVER", width/3, height/1.4);
    noLoop();
  }
}
// for some reason i cant remove this?? 
function keyPressed() {
  if (keyCode === UP_ARROW) {
    ufoPosY-=50;
  } else if (keyCode === DOWN_ARROW) {
    ufoPosY+=50;
  }
//   //reset if the pacman moves out of range
  // if (ufoPosY > mini_height) {
  //   ufoPosY = mini_height;
  // } else if (ufoPosY < 0 - ufoSize.w/2) {
  //   ufoPosY = 0;
  // }
}
